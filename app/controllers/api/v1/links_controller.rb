class Api::V1::LinksController < Api::V1::BaseController 
  
  
  
  def create
    @link = Link.new(:given_url => params[:url])
    return api_error(status: 422, errors: @link.errors) unless @link.valid?

    @link.save!

    render(
      json: Api::V1::LinkSerializer.new(@link).to_json,
      status: 201
    )
  end
  
  private
  def link_params
      params.permit(:url).delete_if{ |k,v| v.nil?}
  end
  
end