json.extract! link, :id, :given_url, :slug, :title, :created_at, :updated_at
json.url link_url(link, format: :json)