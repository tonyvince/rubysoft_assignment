class Api::V1::LinkSerializer < Api::V1::BaseSerializer
  attributes :short_url

  def short_url
    object.display_slug
  end

end