class Link < ActiveRecord::Base
  after_create :generate_slug
  VALID_URL_REGEX = /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}/
  validates :given_url, 
              presence: true, 
              length: { maximum: 255 },
              format: { with: VALID_URL_REGEX, message: "Invalid URL format" }
  def generate_slug
    self.slug = self.id.to_s(36)
    self.save
  end
  def display_slug
    ENV['BASE_URL'] + self.slug
  end
end
