
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Access the source code on cloud9 IDE (https://ide.c9.io/righton/assignment)  - Rails assignment on Cloud9 IDE!

To run the app, just do the following:

1. Run the project with the "Run Project" button in the menu bar on top of the IDE.
2. Preview your new app by clicking on the URL that appears in the Run panel below (https://assignment-righton.c9users.io/).



# URL Shortener #

Implement a URL shortening service in Ruby on Rails

*Service Web Page:*
The service should have a UI where the user can enter a url, which will be shortened.
- It should pick up any URL (should not pick up anything other than URLs)
- The URL should be converted to a shorter form (unique short string) similar to the http://tinyurl.com/ service
- When the user keys in the shorter URL in the browser, service should redirect to the original URL
- Use bootstrap for layout

*Service API:*
Also expose an JSON API for creating short urls via POST with the following request payload

{
  url: 'https://blog.codinghorror.com/lets-encrypt-everything/'
}

which should respond with json for the development server running at port 3000

{
  "short_url": "http://localhost:3000/a5ap8cc83"
}

In case of a validation error the response should be

{
  "error": "Invalid URL format"
}
